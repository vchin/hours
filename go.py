from genericpath import isdir
from os import listdir
from os.path import isfile, join
from pprint import pprint
import datetime

from datetime import timedelta

PATH = '/Users/vadim/Desktop'
TIME_INTERVAL = 20

work_time = timedelta(minutes=0)

for path in listdir(PATH):

    prev_time = None
    start_time = None

    if isdir(join(PATH, path)) and path.startswith('day'):
        path = join(PATH, path)
        day_time = timedelta()

        for f in sorted([ f for f in listdir(path)]):
            # dt = f.split('_')

            # print datetime.datetime.strptime(f, '')
            try:
                if f[2] == '-':
                    dtm = datetime.datetime.strptime(f, '%d-%m-%Y_%H.%M.%S.jpg')
                else:
                    dtm = datetime.datetime.strptime(f, '%Y-%m-%d_%H.%M.%S.jpg')
            except BaseException,e:
                print e
                continue

            if not start_time:
                start_time = dtm

            if prev_time:

                dtt = dtm - prev_time
                if dtt.total_seconds() > TIME_INTERVAL:
                    dlta = prev_time - start_time
                    day_time += dlta
                    work_time += dlta

                    print path, dlta

                    start_time = dtm

            prev_time = dtm

        if prev_time:
            dlta = prev_time - start_time
            day_time += dlta
            work_time += dlta

        print path, dlta         \

        print '--' * 10, path, ':', day_time, '--' * 10,'\n\r\n\r'


print work_time, ':', work_time.days + work_time.seconds / 3600.0
